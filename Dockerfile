# 
FROM python:3.10.2-slim-bullseye

# 
WORKDIR /app

# 
COPY . .

# 
RUN pip install --no-cache-dir --upgrade -r requirements.txt

# 
EXPOSE 50050
CMD ["python", "server.py"]
