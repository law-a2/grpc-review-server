import grpc
import review_pb2
import review_pb2_grpc
from concurrent import futures
import time
import uuid

all_reviews = []


class ReviewServicer(review_pb2_grpc.ReviewServiceServicer):
    def GetAllReview(self, request, context):
        response = review_pb2.AllReviewResponse()
        response.review.extend(all_reviews)
        return response

    def CreateReview(self, request, context):
        response = review_pb2.ReviewDto()
        all_reviews.append(request.review)
        response.review = request.review
        return response


def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    review_pb2_grpc.add_ReviewServiceServicer_to_server(ReviewServicer(), server)

    print("Starting server. Listening on port 50050.")
    server.add_insecure_port("[::]:50050")
    server.start()

    try:
        while True:
            time.sleep(86400)

    except KeyboardInterrupt:
        server.stop(0)


if __name__ == "__main__":
    main()
